<?php

// src/Form/ProfileFormType.php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class FormType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => $this->translator->trans('form.firstname'),
            ])
            ->add('lastname', TextType::class, [
                'label' => $this->translator->trans('form.lastname'),
            ])
            ->add('description', TextType::class, [
                'label' => $this->translator->trans('form.description'),
            ]);
    }
}

