<?php

namespace App\Controller;

use App\Form\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends AbstractController
{
    /**
     * @Route("/profile/edit", name="profile_edit")
     */
    public function edit(Request $request): Response
    {
        $form = $this->createForm(FormType::class);

        $form->handleRequest($request);

        return $this->render('profile/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
